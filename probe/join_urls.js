/**
 *
 * @param {URL} base
 * @param {URL} ref
 * @returns
 */
function mergeUrls(base, ref) {
    const url = new URL('', base)

    if (url.pathname.endsWith('/')) {
        url.pathname += ref.pathname.slice(1)
    }  else {
        url.pathname += ref.pathname
    }

    // merge query params
    if (ref.search) {
        if (url.search) {
            url.search = `${url.search}&${ref.search.slice(1)}`
        } else {
            url.search = ref.search
        }
    }

    // replace fragment
    if (ref.hash) {
        url.hash = ref.hash
    }

    return url
}


const bases = [
    "http://webrtc.localhost",
    "http://webrtc.localhost/",
    "http://webrtc.localhost/1001",
    "http://webrtc.localhost/?cid=1001",
]

const refs = [
    "/test",
    "test/relative",
    "http://app.localhost/absolute",
    "../goback",
    "/something with spaces",
    "/qr?text=hello",
]

function test(base, ref) {
    const baseUrl = new URL(base)
    const refUrl = new URL(ref, baseUrl)

    console.log(base, ref)
    console.log(`mergeUrls(${baseUrl}, ${refUrl}) = ${mergeUrls(baseUrl, refUrl)}`)
    console.log(`new URL(${refUrl}, ${baseUrl}) = ${new URL(refUrl, baseUrl)}`)
    console.log()
}

for (const base of bases) {
    for (const ref of refs) {
        test(base, ref)
    }
}
