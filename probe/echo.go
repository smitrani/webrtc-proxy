package main

import (
	"os"
	"fmt"
	"io"
	"net/http"
)

func main () {
	listen := ":9999"

	if len(os.Args) == 2 {
		listen = os.Args[1]
	} else {
		fmt.Fprintf(os.Stderr, "Usage: %s <listen>\n", os.Args[0])
		os.Exit(1)
	}

	http.ListenAndServe(listen, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		w.WriteHeader(200)
		io.Copy(w, r.Body)
	}))
}
