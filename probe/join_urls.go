package main

import (
	"fmt"
	urlpkg "net/url"
	"path"
)

func mergeUrls(base *urlpkg.URL, ref *urlpkg.URL) *urlpkg.URL {
	relPath := ref.Path

	// make sure path is absolute -- this makes Clean() drop leading .. segments
	if relPath[0] != '/' {
		relPath = "/" + relPath
	}

	relPath = path.Clean(relPath)

	url := base.JoinPath(relPath)

	// merge queries
	if ref.ForceQuery {
		url.ForceQuery = true
	}

	if ref.RawQuery != "" {
		if url.RawQuery == "" {
			url.RawQuery = ref.RawQuery
		} else {
			url.RawQuery = fmt.Sprintf("%s&%s", url.RawQuery, ref.RawQuery)
		}
	}

	// replace fragment if it exists in ref
	if ref.Fragment != "" {
		url.Fragment = ref.Fragment
		url.RawFragment = ref.RawFragment
	}

	return url
}

func test(base, ref string) {
	baseUrl, err := urlpkg.Parse(base)
	if err != nil {
		fmt.Println(err)
		return
	}

	refUrl, err := urlpkg.Parse(ref)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("mergeUrls(%s, %s) = %s\n", baseUrl, refUrl, mergeUrls(baseUrl, refUrl))
	fmt.Printf("%s.JoinPath(%s) = %s\n", baseUrl, ref, baseUrl.JoinPath(path.Clean(ref)))
	fmt.Printf("%s.ResolveReference(%s) = %s\n", baseUrl, refUrl, baseUrl.ResolveReference(refUrl))
	fmt.Println()
}

func main() {
	bases := []string{
		"http://webrtc.localhost",
		"http://webrtc.localhost/",
		"http://webrtc.localhost/1001",
		"http://webrtc.localhost/?cid=1001",
	}

	refs := []string{
		"/test",
		"test/relative",
		"http://app.localhost/absolute",
		"../goback",
		"/something with spaces",
		"/qr?text=hello",
	}

	for _, base := range bases {
		for _, ref := range refs {
			test(base, ref)
		}
	}
}
