package main

import (
	"bytes"
	// "context"
	"fmt"
	"io"
	"net/http"
	"net/http/httputil"
	urlpkg "net/url"
	"os"
	"strings"
)

func main() {
	listen := ":6060"
	var urlStr string

	if len(os.Args) == 3 {
		urlStr = os.Args[1]
		listen = os.Args[2]
	} else {
		fmt.Fprintf(os.Stderr, "Usage: %s <url> <listen>\n", os.Args[0])
		os.Exit(1)
	}

	url, err := urlpkg.Parse(urlStr)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err)
		os.Exit(1)
	}

	proxy := httputil.NewSingleHostReverseProxy(url)

	http.ListenAndServe(listen, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		buf := bytes.Buffer{}

		body := NewDebugRequestWriter(r, NewPrefixWriter(&buf, "> "))

		responseWriter := NewDebugResponseWriter(w, NewPrefixWriter(&buf, "< "))

		r.Host = url.Host
		r.Body = body
		proxy.ServeHTTP(responseWriter, r)

		responseWriter.Close()
		fmt.Println(buf.String())
	}))
}

type DebugRequestWriter struct {
	Request *http.Request
	Debug   io.Writer

	body          io.ReadCloser
	headerWritten bool
}

func NewDebugRequestWriter(r *http.Request, debug io.Writer) *DebugRequestWriter {
	return &DebugRequestWriter{
		Request: r,
		Debug:   debug,

		body: r.Body,
	}
}

func (rw *DebugRequestWriter) writeHeader() {
	r := rw.Request
	// TODO: we need to print the query and the fragment as well
	fmt.Fprintf(rw.Debug, "%s %s %s\n", r.Method, r.URL.Path, r.Proto)

	for name, vals := range r.Header {
		for _, val := range vals {
			fmt.Fprintf(rw.Debug, "%s: %s\n", name, val)
		}
	}

	fmt.Fprintf(rw.Debug, "\n")

	rw.headerWritten = true
}

func (rw *DebugRequestWriter) Read(p []byte) (n int, err error) {
	if !rw.headerWritten {
		rw.writeHeader()
	}

	n, err = rw.body.Read(p)
	if n > 0 {
		if n, err := rw.Debug.Write(p[:n]); err != nil {
			return n, err
		}
	}
	return
}

func (rw *DebugRequestWriter) Close() error {
	if !rw.headerWritten {
		rw.writeHeader()
	}

	if debugCloser, ok := rw.Debug.(io.Closer); ok {
		debugCloser.Close()
	}

	return rw.body.Close()
}

type DebugResponseWriter struct {
	http.ResponseWriter
	Debug io.Writer
}

func NewDebugResponseWriter(w http.ResponseWriter, debug io.Writer) *DebugResponseWriter {
	return &DebugResponseWriter{
		ResponseWriter: w,
		Debug:          debug,
	}
}

func (rw *DebugResponseWriter) Write(p []byte) (n int, err error) {
	n, err = rw.ResponseWriter.Write(p)
	rw.Debug.Write(p)
	return
}

func (rw *DebugResponseWriter) Close() error {
	responseCloser, ok := rw.ResponseWriter.(io.Closer)
	if ok {
		if err := responseCloser.Close(); err != nil {
			return err
		}
	}

	debugCloser, ok := rw.Debug.(io.Closer)
	if ok {
		if err := debugCloser.Close(); err != nil {
			return err
		}
	}

	return nil
}

func (rw *DebugResponseWriter) WriteHeader(statusCode int) {
	headers := rw.Header().Clone()
	rw.ResponseWriter.WriteHeader(statusCode)

	fmt.Fprintf(rw.Debug, "%d %s\n", statusCode, http.StatusText(statusCode))

	for name, vals := range headers {
		for _, val := range vals {
			fmt.Fprintf(rw.Debug, "%s: %s\n", name, val)
		}
	}

	fmt.Fprintf(rw.Debug, "\n")
}

type PrefixWriter struct {
	Writer io.Writer
	Prefix string
	buffer string // To store incomplete lines
}

func NewPrefixWriter(w io.Writer, prefix string) *PrefixWriter {
	return &PrefixWriter{
		Writer: w,
		Prefix: prefix,
	}
}

func (pw *PrefixWriter) Write(p []byte) (n int, err error) {
	input := pw.buffer + string(p)

	// we need to handle this case specifically here to be able to
	// differentiate between empty lines (because it's the last one and the
	// input ends with a NL) and empty lines (because they are part of  the
	// actual data we want to write)
	input, endsWithNl := strings.CutSuffix(input, "\n")

	lines := strings.Split(input, "\n")

	for i, line := range lines {

		// If this is not the last line, add the prefix and write it
		if i < len(lines)-1 || endsWithNl {
			if err := pw.printLine(line); err != nil {
				return n, err
			}
		} else {
			// If this is the last line and it doesn't end with a newline,
			// store it in the buffer for future writes
			pw.buffer = line
		}

		n += len(line) + 1
	}

	return len(p), nil
}

func (pw *PrefixWriter) Close() error {
	if len(pw.buffer) == 0 {
		return nil
	}

	if err := pw.printLine(pw.buffer); err != nil {
		return err
	}

	pw.buffer = ""
	return nil
}

func (pw *PrefixWriter) printLine(line string) error {
	_, err := fmt.Fprintf(pw.Writer, "%s%s\n", pw.Prefix, line)
	return err
}
