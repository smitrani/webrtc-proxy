const plugin = require('tailwindcss/plugin')

/** @type {import('tailwindcss').Config} */
export default {
    content: [
        'src/**/*.tsx'
    ],
    theme: {
        extend: {},
    },
    plugins: [
        plugin(({ addVariant }) => {
            addVariant('hx', '.htmx-request &')
        })
    ],
}

