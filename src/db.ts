/// <reference lib="webworker" />

declare const self: ServiceWorkerGlobalScope

import * as idb from 'idb-keyval'
import { getClient } from '@/broadcast'

// interface

const KEY_CLIENT = 'client'

export async function getLastConnectedClient(): Promise<Client | undefined> {
    const clientId = await get<string>(KEY_CLIENT)
    if (!clientId) {
        return
    }

    try {
        const client = await getClient(clientId)
        return client
    } catch {
        return
    }
}

export async function setLastConnectedClient(client: Client): Promise<void> {
    await set(KEY_CLIENT, client.id)
}

export async function deleteLastConnectedClient(): Promise<void> {
    await del(KEY_CLIENT)
}

// implementation

const store = idb.createStore('_proxy', '_proxy')

function set(key: string, val: any): Promise<void> {
    return idb.set(key, val, store)
}

function get<T>(key: string): Promise<T|undefined> {
    return idb.get(key, store)
}

// function keys(): Promise<string[]> {
//     return idb.keys(store)
// }

function del(key: string): Promise<void> {
    return idb.del(key, store)
}
