/// <reference types="vite/client" />

declare const __BUILD_TIMESTAMP__ : number

interface ImportMetaEnv {
    readonly PUSHER_KEY: string
    readonly PUSHER_CLUSTER: string
    readonly FUNCTIONS_BASE_URL: string
}

interface ImportMeta {
    readonly env: ImportMetaEnv
}
