export class HttpError extends Error {
    constructor(
        public readonly statusCode: number,
        public readonly headers?: HeadersInit,
        public readonly body?: BodyInit
    ) {
        super()
    }

    toResponse(): Response {
        return new Response(this.body, {
            status: this.statusCode,
            headers: this.headers
        })
    }
}

export function error(statusCode: number, body?: BodyInit, headers?: HeadersInit): never {
    throw new HttpError(statusCode, headers, body)
}

export function redirect(href: string, statusCode = 302): never {
    const isAbsolute = /^https?:\/\//.test(href)
    const absoluteUrl = isAbsolute ? href : `${location.origin}/${href.replace(/^\//, '')}`

    throw new HttpError(statusCode, {
        location: absoluteUrl
    }, absoluteUrl)
}
