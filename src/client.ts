import Pusher, { Channel } from 'pusher-js'
import pWorker from '@jreusch/p-worker'
import {
    createPeerConnection,
    createDataChannel,
    createPusherSignalingChannel,
    createWriteStream,
    createReadStream,
    receiveBlob
} from '@jreusch/webrtc'
import pDestruct from '@jreusch/p-destruct'

import {
    ClientId,
    RequestData,
    ResponseData,
    ServerId,
    newClientId,
    onClientMessage
} from '@/broadcast'

// unfortunately, we cannot do the same ?worker trick here :(
// let's just hope that vite will do the "right thing"
import '@/htmx'
import '@/main.css'

// number of channels per connection
// most browsers limit concurrency to 6-8
const concurrency = 8

// timeout for inquire - if we don't get an answer after this time,
// we assume id is invalid, or the server has been stopped
const timeout = 5 * 1000

// Pusher.logToConsole = true
const pusher = new Pusher(import.meta.env.PUSHER_KEY, {
    cluster: import.meta.env.PUSHER_CLUSTER,
    channelAuthorization: {
        transport: 'ajax',
        endpoint: '/_proxy/auth'
    }
})


// client state
let publicChannel : Channel | undefined

interface ConnectionState {
    abort(): void
    request(request: RequestData): Promise<ResponseData>
}

type Connections =
    | undefined // not yet in either mode
    | Map<ClientId, ConnectionState> // server/listen mode
    | ConnectionState // client/connect mode

let connections: Connections = undefined

function matchConnections<T>(
    onNotConnected: () => T,
    onServer: (clients: Map<ClientId, ConnectionState>) => T,
    onClient: (state: ConnectionState) => T
): T {
    if (connections instanceof Map) {
        return onServer(connections)
    } else if (connections) {
        return onClient(connections)
    } else {
        return onNotConnected()
    }
}

// update

// if we unload somehow, notify the server
window.addEventListener('pagehide', () => {
    disconnect()
    navigator.sendBeacon('/_proxy/unload')
})

// if we find an update, always reload to apply it - this is mostly a a dev thing..
navigator.serviceWorker.addEventListener('controllerchange', () => {
    location.reload()
})

navigator.serviceWorker.addEventListener('message', onClientMessage(async msg => {
    switch (msg.tag) {
        case 'listen': return await listen(msg.server, msg.url)
        case 'connect': return await connect(msg.server)
        case 'disconnect': return disconnect()
        case 'inquire': return await inquire(msg.server)
        case 'request': return await matchConnections(
            () => { throw new Error('Not connected') },
            () => { throw new Error('Got a request in server mode') },
            (state) => state.request(msg.request)
        )
    }

    msg satisfies never;
}))

function disconnect() {
    // clear connections and close them
    matchConnections(
        () => {},
        clients => {
            for (const client of clients.values()) {
                client.abort()
            }
        },
        state => {
            state.abort()
        }
    )
    connections = undefined

    // close public channel
    if (publicChannel) {
        publicChannel.unbind_all()
        pusher.unsubscribe(publicChannel.name)
        publicChannel = undefined
    }
}

async function listen(serverId: ServerId, url: string) {
    disconnect()

    publicChannel = await connectToChannel(serverId)

    publicChannel.bind('client-inquire', async () => {
        publicChannel?.trigger('client-announce', { url: url })
    })

    const clients = new Map<ClientId, ConnectionState>()
    connections = clients

    publicChannel.bind('client-connect', async ({ from }: { from: ClientId }) => {
        // abort already existing connection
        clients.get(from)?.abort()

        const abort = new AbortController()
        const signal = abort.signal

        signal.addEventListener('abort', () => {
            clients.delete(from)
        })

        const privateChannel = await connectToChannel(from, signal)
        const connection = connectToPeer(privateChannel, abort)

        connection.addEventListener('datachannel', ({ channel }: RTCDataChannelEvent) => {
            signal.addEventListener('abort', () => {
                channel.close()
            })

            receiveRequests(url, channel, signal)
        })

        privateChannel.trigger('client-connected', { url })
    })
}

async function connect(serverId: ServerId) {
    disconnect()

    const abort = new AbortController()
    abort.signal.addEventListener('abort', () => {
        disconnect()
    })

    const result = await Promise.race([
        connectHelp(serverId, abort).catch(() => null),
        delay(timeout, null)
    ])

    if (!result) {
        abort.abort()
    }

    return result
}

async function connectHelp(serverId: ServerId, abort: AbortController) {
    const signal = abort.signal
    // we only use this client id for the remote connection, we use the browser's
    // one everywhere else!
    const clientId = newClientId()

    const [publicChannel, privateChannel] = await Promise.all([
        connectToChannel(serverId, signal),
        connectToChannel(clientId, signal)
    ])

    // step 1: setup local webrtc, but don't trigger connecting yet
    signal.throwIfAborted()
    const connection = connectToPeer(privateChannel, abort)

    // step 2: advertise a new client on the public channel
    publicChannel.trigger('client-connect', { from: clientId })

    // step 3: wait for the server to have opened their side of the webrtc channel
    const { url } = await new Promise<{ url: string }>((res, rej) => {
        const cleanup = () => {
            privateChannel.unbind('client-connected')
            publicChannel.unbind_all()
            pusher.unsubscribe(publicChannel.name)
        }

        signal.addEventListener('abort', () => {
            cleanup()
            rej(signal.reason)
        })

        privateChannel.bind('client-connected', (data: { url: string }) => {
            cleanup()
            res(data)
        })
    })


    // step 4: create the data channels, triggering the connect
    signal.throwIfAborted()
    const dataChannels = await Promise.all(Array.from({ length: concurrency }, () =>
        createDataChannel(connection, {
            signal,
            ordered: true
        })))

    // step 5: setup the internal data structure
    signal.throwIfAborted()

    const worker = pWorker(dataChannels)
    connections = {
        abort () {
            abort.abort()
        },

        request(request: RequestData): Promise<ResponseData> {
            // This one is tricky: sendRequest needs to block until the read the
            // entire response, such that worker will lock the channel the
            // entire time, and we don't accidentily end up reusing it.
            //
            // We still wanna early-return with a ReadableStream instead, though
            const result = pDestruct<ResponseData>()

            worker(async channel => {
                try {
                    const response = await sendRequest(channel, request, signal)

                    const [body, bodyCopy] = response.body.tee()
                    // resolve early, but replace the body with our teed version
                    result.resolve({ ...response, body })

                    // wait for the stream to close, by just piping it to an
                    // "empty/null" WritableStream
                    await bodyCopy.pipeTo(new WritableStream())
                } catch(err) {
                    result.reject(err)
                }

            })

            return result.promise
        }
    }

    return { url, clientId }
}

async function inquire(serverId: ServerId) {
    try {
        const publicChannel = await connectToChannel(serverId)

        publicChannel.trigger('client-inquire', '')
        const result = await Promise.race([
            new Promise<{ url: string }>((res) => publicChannel.bind('client-announce', res)),
            delay(timeout, null)
        ])

        publicChannel.unbind_all()
        pusher.unsubscribe(publicChannel.name)

        return result
    } catch {
        return null
    }
}

// implementation

function connectToPeer(privateChannel: Channel, abort: AbortController): RTCPeerConnection {
    const signaling = createPusherSignalingChannel(privateChannel)
    const connection = createPeerConnection({
        signal: abort.signal,
        signaling: signaling,
        autoRestartIce: true,

        iceServers: [
            { urls: 'stun:stun.l.google.com:19302' },
            {
                urls: [
                    "turn:eu-0.turn.peerjs.com:3478",
                    "turn:us-0.turn.peerjs.com:3478",
                ],
                username: "peerjs",
                credential: "peerjsp",
            }
        ]
    })

    connection.addEventListener('connectionstatechange', () => {
        // console.debug('Connection state: ', connection.connectionState)

        // we try to update the state and related resources automatically
        // using lifecycle events -
        // so if we disconnect, unsubscribe from the signaling channel
        // and also remove ourselfes from the connections array.
        if (connection.connectionState === 'closed') {
            abort.abort()
        }
    })

    return connection
}

function connectToChannel(id: ServerId | ClientId, signal?: AbortSignal): Promise<Channel> {
    const channel = pusher.subscribe('private-' + id)
    signal?.addEventListener('abort', () => {
        channel.unbind_all()
        pusher.unsubscribe(channel.name)
    })

    if (channel.subscribed) {
        return Promise.resolve(channel)
    }

    return new Promise((res, rej) => {
        channel.bind('pusher:subscription_succeeded', () => res(channel))
        channel.bind('pusher:subscription_error', rej)
        signal?.addEventListener('abort', () => rej)
    })
}

interface RequestHeader {
    url: string
    method: string
    headers: Record<string, string>
    hasBody: boolean
}

async function sendRequest(channel: RTCDataChannel, request: RequestData, signal: AbortSignal): Promise<ResponseData> {
    signal.throwIfAborted()

    const header = {
        url: request.url,
        method: request.method,
        headers: request.headers,
        hasBody: !!request.body
    } satisfies RequestHeader

    console.log(`${request.method} ${request.url} HTTP/1.1\n${Object.entries(request.headers).map(([k,v]) => `${k}: ${v}`).join('\n')}`)

    channel.send(JSON.stringify(header))

    if (request.body) {
        const requestBody = createWriteStream(channel)
        request.body.pipeTo(requestBody, { signal })
    }

    // first chunk: status, headers
    const firstMsg = await new Promise<string>(res =>
        channel.addEventListener('message', ev => res(ev.data), { once: true, signal }))

    const res: { status: number, headers: Record<string,string> } = JSON.parse(firstMsg)

    const body = createReadStream(channel)
    signal.addEventListener('abort', () => {
        body.cancel(signal.reason)
    })

    return { ...res, body }
}


async function receiveRequests(baseUrl: string, channel: RTCDataChannel, signal: AbortSignal) {
    while (!signal.aborted) {
        const firstMsg: string = await new Promise((res) =>
            channel.addEventListener('message', e => res(e.data), { once: true, signal }))

        const header: RequestHeader = JSON.parse(firstMsg)

        // we CANNOT stream here, passing a ReadableStream to blob would cause
        // a CORS preflight request to happen
        let requestBody: Blob|null = null
        if (header.hasBody) {
            requestBody = await receiveBlob(channel, { signal })
        }

        const filteredHeaders: Record<string,string> = {}
        for (const [headerName, headerValue] of Object.entries(header.headers)) {
            if (!isCorsSafeHeader(headerName, headerValue)) {
                continue
            }

            // no need to re-write referer, since it is not a CORS-safe header.
            filteredHeaders[headerName] = headerValue
        }

        const requestUrl = mergeUrls(baseUrl, header.url)
        const res = await fetch(requestUrl, {
            signal,
            method: header.method,
            headers: filteredHeaders,
            body: requestBody,
            mode: 'cors',
            // credentials: 'include'
        })

        channel.send(JSON.stringify({
            status: res.status,
            headers: Object.fromEntries(res.headers)
        }))

        const writeStream = createWriteStream(channel)

        // response.body is always supported
        const body = res.body ?? new Blob([]).stream()

        await body.pipeTo(writeStream, { signal })
    }
}


function isCorsSafeHeader(name: string, value: string): boolean {
    // https://fetch.spec.whatwg.org/#cors-safelisted-request-header
    if (value.length > 128) {
        // NOTE: this excludes Chrome's default Accept header
        // text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7
        return false
    }

    switch (name.toLowerCase()) {
        case 'accept':
            return !containsCorsUnsafeRequestHeaderBytes(value);

        case 'accept-language':
        case 'content-language':
            return !/[0-9A-Za-z\x\x20*,.;=-]/.test(value)

        case 'content-type':
            // TODO: this is not correct, but w/e
            return ['application/x-www-form-urlencoded', 'multipart/form-data', 'text/plain'].includes(value)

        case 'range':
            // range headers might generally be allowed, but we just don't, beacuse it's easier.
            return false

        default:
            return false
    }
}

function containsCorsUnsafeRequestHeaderBytes(value: string): boolean {
    // https://fetch.spec.whatwg.org/#cors-unsafe-request-header-byte
    return /[\0-\x08\x0A-\x1F\)\(:<>?@\[\]\\\{\}\x7F]/.test(value)
}


function mergeUrls(base: URL|string, ref: URL|string): URL {
    if (!(ref instanceof URL)) {
        ref = new URL(ref, base)
    }

    // we want to make a copy of base to not modify our inputs
    const url = new URL('', base)

    // the URL constructor handles '..' correctly for us
    if (url.pathname.endsWith('/')) {
        url.pathname += ref.pathname.slice(1)
    }  else {
        url.pathname += ref.pathname
    }

    // merge query params
    if (ref.search) {
        if (url.search) {
            url.search = `${url.search}&${ref.search.slice(1)}`
        } else {
            url.search = ref.search
        }
    }

    // replace fragment
    if (ref.hash) {
        url.hash = ref.hash
    }

    return url
}

function delay<T>(ms: number, value: T): Promise<T> {
    return new Promise(res => setTimeout(res, ms, value))
}
