import { VNode, ComponentChildren } from "preact"

export default function Link(props: { href: string, children: ComponentChildren }): VNode {
    const isExternal = !props.href.startsWith('/_proxy')

    return (
        <a
            class="text-purple-800 hover:underline focus:underline focus:outline-none"
            href={props.href}
            target={isExternal ? '_blank' : void 0}
            rel={isExternal ? "noopener noreferrer nofollow" : void 0}
        >
            {props.children}
        </a>
    )
}
