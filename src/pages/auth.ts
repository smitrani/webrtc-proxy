export default async function handlePusherAuth(_params: unknown, e: FetchEvent): Promise<Response> {
    const req = e.request
    // need to use blob here - FormData is not serialized properly?
    const body = await req.blob()
    return await fetch(`${import.meta.env.FUNCTIONS_BASE_URL}/auth`, {
        method: req.method,
        headers: req.headers,
        body: body
    })
}

