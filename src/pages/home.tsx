import { VNode } from "preact";

import { error } from "@/http-error";
import { disconnect, getClient, listen, newServerId } from "@/broadcast";

import Layout from "@/components/Layout";
import Button from "@/components/Button";
import Input from "@/components/Input";
import Link from "@/components/Link";


export async function get(_params: unknown, _e: FetchEvent): Promise<VNode> {
    return <Layout title="Proxy a website, using WebRTC">

        <ProxyForm />

        <h2 class="text-2xl font-bold mt-12 mb-6 text-purple-800">What is this?</h2>

        <p class="text-lg text-semibold mt-4">
            Ever wanted to share your local version of a project with a colleague or your parents?
        </p>

        <p class="mt-4">
            This website allows you to do just that, without setting up complicated ssh forwarding tunnels, or preview branch deploys. Instead, it uses WebRTC (the technology used for video calls) to send requests and responses directly and securely between browsers, no server required!
        </p>

        <p class="mt-4">
            Starting a proxy creates a unique link, that when sent to the other person reconfigures this webpage in their browser to send all requests to your browser instead. Your browser will then fetch the requested page, and send the response back.
        </p>

        <p class="mt-4">
            <strong>Note:</strong> Using the browser version of the server <Link href="https://gitlab.com/arkandos/webrtc-proxy#no-need-to-worry-about-cors">has a few limitations.</Link> If you run into CORS problems or require cookies, you can also try <Link href="https://gitlab.com/arkandos/webrtc-proxy/-/releases">the CLI server</Link> instead!
        </p>
    </Layout>
}

export async function post(_params: unknown, e: FetchEvent): Promise<VNode> {
    const client = await getClient(e.clientId)

    const body = await e.request.formData()

    const targetUrl = body.get('url')
    if (!targetUrl || typeof targetUrl !== 'string') {
        error(400)
    }

    const serverId = newServerId()
    await listen(client, serverId, targetUrl)

    const proxyUrl = `${location.origin}/_proxy/${serverId}`

    return <form
        class="my-16"
        hx-delete="/_proxy"
        hx-swap="outerHTML"
    >
        <p class="font-semibold text-lg">
            <span class="inline-block w-4 h-4 align-middle rounded-full bg-green-600 mr-6" />
            Proxy started.
        </p>

        <div class="md:flex flex-row-reverse items-center gap-4">
            <img
                className="hidden md:block mt-6 w-32 h-32"
                src={`/_proxy/qr?url=${encodeURIComponent(proxyUrl)}`}
            />

            <div class="flex-1">
                <Input
                    name="url"
                    type="url"
                    className="mt-6"
                    label="Copy this URL and send it to the peer:"
                    value={proxyUrl}
                    readonly
                />

                <div class="mt-4 flex gap-4">
                    <Button
                        type="button"
                        onClick={function () {
                            const form: HTMLFormElement = this.closest('form')!
                            const url: string = form['url'].value
                            if (
                                typeof navigator.share === 'function'
                                && typeof navigator.canShare === 'function'
                                && navigator.canShare({ url })
                            ) {
                                navigator.share({ url })
                            } else {
                                navigator.clipboard.writeText(url)
                            }
                        }}
                    >
                        Copy URL
                    </Button>

                    <Button type="submit">
                        Stop Proxy
                    </Button>
                </div>
            </div>
        </div>
    </form>
}

export async function delet(_params: unknown, e: FetchEvent) {
    const client = await getClient(e.clientId)
    await disconnect(client)

    return <ProxyForm />
}

function ProxyForm() {
    return <form
        class="my-16"
        hx-post="/_proxy"
        hx-swap="outerHTML"
    >
        <Input
            name="url"
            type="url"
            className="mt-4"
            label="Enter the domain here to generate a share link:"
            placeholder="http://project.localhost"
            required
        />

        <div class="mt-4">
            <Button type="submit">Start Proxy</Button>
        </div>
    </form>
}
