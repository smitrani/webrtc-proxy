/// <reference lib="webworker" />

declare const self: ServiceWorkerGlobalScope

const clientCacheName = '_proxy' + __BUILD_TIMESTAMP__

export async function onActivate() {
    await self.clients.claim()

    // delete other (older) caches.
    const existingCacheNames = await caches.keys()
    await Promise.allSettled(existingCacheNames.map(cacheName =>
        cacheName === clientCacheName
            ? Promise.resolve()
            : caches.delete(cacheName)
    ))
}

export async function get (_params: unknown, e: FetchEvent): Promise<Response> {
    const cache = await caches.open(clientCacheName)
    const cacheRes = await cache.match(e.request)
    if (cacheRes) {
        return cacheRes
    }

    console.warn("Not found in cache: ", e.request.url)

    const fetchRes = await fetch(e.request)
    cache.put(e.request, fetchRes.clone())

    return fetchRes
}
