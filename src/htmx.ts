import * as htmx from 'htmx.org'

declare global {
    interface Window {
        htmx: typeof htmx
    }
}

// interface HtmxConfigRequestDetails {
//     parameters: Record<string, string>
//     headers: Record<string, string>
// }

window.htmx = htmx
export default htmx
