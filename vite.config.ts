import { resolve, dirname } from 'path'
import { fileURLToPath } from 'url'
import { defineConfig } from 'vite'

const baseDir = dirname(fileURLToPath(import.meta.url))

export default defineConfig({
    resolve: {
        alias: {
            '@': resolve(baseDir, 'src')
        }
    },

    define: {
        __BUILD_TIMESTAMP__: (Date.now() / 1000)|0
    },

    envPrefix: [
        'PUSHER_KEY',
        'PUSHER_CLUSTER',
        'FUNCTIONS_BASE_URL'
    ],

    build: {
        rollupOptions: {
            input: {
                main: resolve(baseDir, 'index.html'),
                sw: resolve(baseDir, 'src/service-worker.ts')
            },

            // TODO: are these options even used?
            // changing these to from _proxy/ to _proxy/assets/ did not change anything
            output: {
                assetFileNames: '_proxy/assets/[name].[hash].[ext]',
                chunkFileNames: '_proxy/assets/[name].[hash].[ext]',
                entryFileNames(chunk) {
                    if (chunk.name === 'sw') {
                        return '_proxy-sw.js'
                    } else {
                        return '_proxy/assets/[name].[hash].js'
                    }
                }
            }
        },
    },

    worker: {
        rollupOptions: {
            output: {
                assetFileNames: '_proxy/assets/[name].[hash].[ext]',
                chunkFileNames: '_proxy/assets/[name].[hash].[ext]',
                entryFileNames: '_proxy/assets/[name].[hash].js',
            }
        }
    }
})
