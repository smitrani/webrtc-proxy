package main

import (
	"flag"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strings"
	"unicode"

	"github.com/google/uuid"
	"github.com/lizilong007/pusher-ws-go"
	"github.com/pion/webrtc/v4"
	"github.com/skip2/go-qrcode"
)

const (
	PUSHER_CLUSTER     = "eu"
	PUSHER_KEY         = "4061666fb152427ae9a4"
	FUNCTIONS_BASE_URL = "https://proxy.joshi.monster/.netlify/functions"
)

func usage() {
	f := flag.CommandLine.Output()
	fmt.Fprintf(f, "Usage: %s [options] <URL>\nOptions:\n\n", os.Args[0])
	flag.PrintDefaults()
	fmt.Fprintf(f, "  -help\n    \tPrint this help\n\n")
}

func main() {
	printQr := false
	useTurnServers := true
	id := ""
	headers := make(http.Header)

	flag.BoolVar(&printQr, "qr", printQr, "Print a QR code with the link")
	flag.BoolVar(&useTurnServers, "turn", useTurnServers, "Use TURN servers if no P2P connection is possible")
	flag.StringVar(&id, "id", id, "Use a custom ID instead of auto-generating one")
	flag.Var(headerValue(headers), "H", "pass an additional header to every request")
	flag.Var(headerValue(headers), "header", "pass an additional header to every request")

	flag.Usage = usage
	flag.Parse()

	if flag.NArg() != 1 {
		flag.Usage()
		os.Exit(1)
	}

	url, err := url.Parse(flag.Arg(0))
	if err != nil {
		fmt.Fprintf(flag.CommandLine.Output(), "Invalid URL: %s\n", err)
		os.Exit(1)
	}

	client := pusher.Client{
		Cluster: PUSHER_CLUSTER,
		AuthURL: FUNCTIONS_BASE_URL + "/auth",
	}

	err = client.Connect(PUSHER_KEY)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err)
		os.Exit(1)
	}

	iceServers := []webrtc.ICEServer{
		{
			URLs: []string{"stun:stun.l.google.com:19302"},
		},
	}

	if useTurnServers {
		iceServers = append(iceServers, webrtc.ICEServer{
			URLs: []string{"turn:eu-0.turn.peerjs.com:3478", "turn:us-0.turn.peerjs.com:3478"},

			Username:       "peerjs",
			Credential:     "peerjsp",
			CredentialType: webrtc.ICECredentialTypePassword,
		})
	}

	id = strings.TrimFunc(id, unicode.IsSpace)
	if id == "" {
		id = uuid.NewString()
	}

	proxy := WebrtcProxyServer{
		Id:         id,
		Url:        url,
		Header:     headers,
		IceServers: iceServers,

		Pusher: &client,
	}

	proxyUrl := fmt.Sprintf("https://proxy.joshi.monster/_proxy/%s", proxy.Id)
	// proxyUrl := fmt.Sprintf("http://webrtc-proxy.localhost/_proxy/%s", proxy.Id)
	fmt.Printf("Proxy started, go to %s\n", proxyUrl)

	if printQr {
		if qr, err := qrcode.New(proxyUrl, qrcode.Low); err == nil {
			qr.DisableBorder = true
			fmt.Print(qr.ToSmallString(true))
		}
	}

	if err := proxy.Listen(); err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err)
		os.Exit(1)
	}
}

type headerValue http.Header

func (s headerValue) String() string {
	b := strings.Builder{}
	http.Header(s).Write(&b)
	return b.String()
}

func (s headerValue) Set(text string) error {
	name, value, found := strings.Cut(text, ": ")
	if !found {
		return fmt.Errorf("need a ':' separator, followed by at least one space")
	}

	http.Header(s).Add(name, value)
	return nil
}
