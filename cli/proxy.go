package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path"
	"strings"

	"github.com/google/uuid"
	"github.com/lizilong007/pusher-ws-go"
	"github.com/pion/webrtc/v4"
	"golang.org/x/net/publicsuffix"
)

type WebrtcProxyServer struct {
	Id         string
	Url        *url.URL
	Pusher     *pusher.Client
	IceServers []webrtc.ICEServer
	Header     http.Header

	closeChan chan error
}

func (s *WebrtcProxyServer) Close() {
	if s.closeChan != nil {
		s.closeChan <- nil
	}
}

type connectMsg struct {
	From string `json:"from"`
}

type announceMsg struct {
	Url string `json:"url"`
}

func (s *WebrtcProxyServer) Listen() error {
	if s.Id == "" {
		return fmt.Errorf("empty id")
	}

	// this acutally makes sure that calling Close() while we are not running fails
	s.closeChan = make(chan error)
	defer func() { s.closeChan = nil }()

	publicChannelName := "private-" + s.Id
	publicChannel, err := s.Pusher.Subscribe(publicChannelName)
	if err != nil {
		return err
	}

	defer s.Pusher.Unsubscribe(publicChannelName)

	inquireChan := publicChannel.Bind("client-inquire")
	defer publicChannel.Unbind("client-inquire", inquireChan)

	connectChan := publicChannel.Bind("client-connect")
	defer publicChannel.Unbind("client-connect", connectChan)

	for {
		select {
		case err := <-s.closeChan:
			return err

		case <-inquireChan:
			err = publicChannel.Trigger("client-announce", announceMsg{Url: s.Url.String()})
			if err != nil {
				return err
			}

		case rawConnectMsg := <-connectChan:
			var connectMsg connectMsg
			if err = unmarshal2(rawConnectMsg, &connectMsg); err != nil {
				return err
			}

			connection, err := s.onConnect(connectMsg)
			if err != nil {
				return err
			}

			defer connection.Close()
		}
	}
}

func (s *WebrtcProxyServer) onConnect(msg connectMsg) (*peerConnection, error) {
	addr, err := uuid.Parse(msg.From)
	if err != nil {
		return nil, err
	}

	conn := peerConnection{
		Proxy: s,
		Addr:  addr,
	}

	go func() {
		// fmt.Println("connected to", addr)
		// defer fmt.Println("connection closed:", addr)
		if err := conn.Listen(); err != nil {
			fmt.Fprintf(os.Stderr, "ERROR: %s\n", err)
		}
	}()

	return &conn, nil
}

func (s *WebrtcProxyServer) resolveUrl(ref *url.URL) *url.URL {
	relPath := ref.Path

	// make sure path is absolute -- this makes Clean() drop leading .. segments
	if relPath[0] != '/' {
		relPath = "/" + relPath
	}

	relPath = path.Clean(relPath)

	url := s.Url.JoinPath(relPath)

	// merge queries
	if ref.ForceQuery {
		url.ForceQuery = true
	}

	if ref.RawQuery != "" {
		if url.RawQuery == "" {
			url.RawQuery = ref.RawQuery
		} else {
			url.RawQuery = fmt.Sprintf("%s&%s", url.RawQuery, ref.RawQuery)
		}
	}

	// replace fragment if it exists in ref
	if ref.Fragment != "" {
		url.Fragment = ref.Fragment
		url.RawFragment = ref.RawFragment
	}

	return url
}

//

type peerConnection struct {
	Proxy *WebrtcProxyServer
	Addr  uuid.UUID

	closeChan  chan error
	httpClient *http.Client
}

func (c *peerConnection) Close() {
	if c.closeChan != nil {
		c.closeChan <- nil
	}
}

func (c *peerConnection) Listen() error {
	c.closeChan = make(chan error)
	// this makes sure that calling Close() while we are not running fails
	defer func() { c.closeChan = nil }()

	// setup http client if it is not set
	if c.httpClient == nil {
		// looking at the code for cookiejar.New, it actually never fails.
		jar, err := NewCookieJar(&CookieJarOptions{PublicSuffixList: publicsuffix.List})
		if err != nil {
			return err
		}

		c.httpClient = &http.Client{Jar: jar}
	}

	// setup private channel
	privateChannelName := "private-" + c.Addr.String()
	privateChannel, err := c.Proxy.Pusher.Subscribe(privateChannelName)
	if err != nil {
		return err
	}

	defer c.Proxy.Pusher.Unsubscribe(privateChannelName)

	// Setup peer connection
	pc, err := webrtc.NewPeerConnection(webrtc.Configuration{
		ICEServers: c.Proxy.IceServers,
	})
	if err != nil {
		return err
	}

	defer pc.Close()

	pc.OnConnectionStateChange(func(state webrtc.PeerConnectionState) {
		switch state {
		case webrtc.PeerConnectionStateClosed:
			c.closeChan <- nil

		case webrtc.PeerConnectionStateFailed:
			c.closeChan <- fmt.Errorf("connection failed")

		default:
			// ignore.
			return
		}
	})

	// setup channels
	pc.OnDataChannel(func(channel *webrtc.DataChannel) {
		msgChan := make(chan webrtc.DataChannelMessage)

		channel.OnMessage(func(msg webrtc.DataChannelMessage) {
			msgChan <- msg
		})
		channel.OnClose(func() {
			close(msgChan)
		})

		go func() {
			// fmt.Printf("Data channel opened for client %s: %s\n", c.Addr, channel.Label())
			// defer fmt.Printf("Data channel closed for client %s: %s\n", c.Addr, channel.Label())
			if err := c.onMessages(channel, msgChan); err != nil {
				fmt.Fprintf(os.Stderr, "ERROR: %s\n", err)
			}
		}()
	})

	// connect webrtc to those channels
	pc.OnNegotiationNeeded(func() {
		if err := c.onNegotiationNeeded(pc, privateChannel); err != nil {
			c.closeChan <- err
		}
	})
	pc.OnICECandidate(func(candidate *webrtc.ICECandidate) {
		if err := c.onOurCandidate(candidate, privateChannel); err != nil {
			c.closeChan <- err
		}
	})

	descriptionChan := privateChannel.Bind("client-description")
	defer privateChannel.Unbind("client-description")

	candidateChan := privateChannel.Bind("client-candidate")
	defer privateChannel.Unbind("client-candidate")

	// webrtc starts their own goroutines, we handle pusher events here.
	// otherwise, we would just be blocking anyways.

	// once we have everything in place, send a "connected" event
	if err = privateChannel.Trigger("client-connected", announceMsg{Url: c.Proxy.Url.String()}); err != nil {
		return err
	}

	for {
		select {
		case err := <-c.closeChan:
			return err

		case rawDescriptionMsg := <-descriptionChan:
			var description webrtc.SessionDescription
			if err := unmarshal2(rawDescriptionMsg, &description); err != nil {
				return err
			}

			if err := c.onPeerDescription(pc, privateChannel, description); err != nil {
				return err
			}

		case rawCandidateMsg := <-candidateChan:
			var candidate webrtc.ICECandidateInit
			if err := unmarshal2(rawCandidateMsg, &candidate); err != nil {
				return err
			}

			if err := c.onPeerCandidate(pc, candidate); err != nil {
				return err
			}
		}
	}
}

func (c *peerConnection) onPeerDescription(pc *webrtc.PeerConnection, channel pusher.Channel, description webrtc.SessionDescription) error {
	if err := pc.SetRemoteDescription(description); err != nil {
		return err
	}

	if description.Type == webrtc.SDPTypeOffer {
		answer, err := pc.CreateAnswer(nil)
		if err != nil {
			return err
		}

		if err = pc.SetLocalDescription(answer); err != nil {
			return err
		}

		if err = channel.Trigger("client-description", answer); err != nil {
			return err
		}
	}

	return nil
}

func (c *peerConnection) onPeerCandidate(pc *webrtc.PeerConnection, candidate webrtc.ICECandidateInit) error {
	if pc.RemoteDescription() != nil {
		if err := pc.AddICECandidate(candidate); err != nil {
			return err
		}
	}
	return nil
}

func (c *peerConnection) onNegotiationNeeded(pc *webrtc.PeerConnection, privateChannel pusher.Channel) error {
	switch pc.SignalingState() {
	case webrtc.SignalingStateStable, webrtc.SignalingStateHaveLocalOffer, webrtc.SignalingStateHaveRemotePranswer:
		offer, err := pc.CreateOffer(nil)
		if err != nil {
			return err
		}

		if err = pc.SetLocalDescription(offer); err != nil {
			return err
		}

	default:
		answer, err := pc.CreateAnswer(nil)
		if err != nil {
			return err
		}

		if err = pc.SetLocalDescription(answer); err != nil {
			return err
		}
	}

	if err := privateChannel.Trigger("client-description", pc.LocalDescription()); err != nil {
		return err
	}

	return nil
}

func (c *peerConnection) onOurCandidate(candidate *webrtc.ICECandidate, channel pusher.Channel) error {
	if candidate != nil {
		if err := channel.Trigger("client-candidate", candidate.ToJSON()); err != nil {
			return err
		}
	}

	return nil
}

type requestData struct {
	Url     string            `json:"url"`
	Method  string            `json:"method"`
	Headers map[string]string `json:"headers"`
	HasBody bool              `json:"hasBody"`
}

func (r requestData) String() string {
	out := strings.Builder{}
	fmt.Fprintf(&out, "%s %s HTTP/1.1\n", r.Method, r.Url)

	// for name, val := range r.Headers {
	// 	fmt.Fprintf(&out, "%s: %s\n", name, val)
	// }

	return out.String()
}

type responseData struct {
	Status  int               `json:"status"`
	Headers map[string]string `json:"headers"`
}

func (c *peerConnection) onMessages(channel *webrtc.DataChannel, msgs <-chan webrtc.DataChannelMessage) error {
	for {
		msg, ok := <-msgs
		if !ok {
			return nil
		}

		if err := c.onMessage(channel, msg, msgs); err != nil {
			return err
		}
	}
}

func (c *peerConnection) onMessage(channel *webrtc.DataChannel, msg webrtc.DataChannelMessage, msgs <-chan webrtc.DataChannelMessage) error {
	var requestData requestData
	if err := json.Unmarshal(msg.Data, &requestData); err != nil {
		return err
	}

	fmt.Print(requestData.String())

	var requestBody io.Reader = nil
	if requestData.HasBody {
		requestBody = &DataChannelReader{Msgs: msgs}
	}

	relativeUrl, err := url.Parse(requestData.Url)
	if err != nil {
		return err
	}

	url := c.Proxy.resolveUrl(relativeUrl)

	req, err := http.NewRequest(requestData.Method, url.String(), requestBody)
	if err != nil {
		return err
	}

	for name, value := range requestData.Headers {
		req.Header.Add(name, value)
	}

	for name, values := range c.Proxy.Header {
		req.Header[name] = values
	}

	// act more like a browser
	req.Header.Set("host", c.Proxy.Url.Host)
	req.Header.Set("origin", fmt.Sprintf("%s://%s", c.Proxy.Url.Scheme, c.Proxy.Url.Host))

	if referer, err := url.Parse(req.Header.Get("referer")); err == nil {
		if referer.Host == url.Host {
			req.Header.Set("referer", c.Proxy.resolveUrl(referer).String())
		}
	}

	res, err := c.httpClient.Do(req)
	if err != nil {
		return err
	}

	defer res.Body.Close()

	responseHeaders := make(map[string]string)
	for key, val := range res.Header {
		responseHeaders[key] = val[0]
	}

	responseData := responseData{
		Status:  res.StatusCode,
		Headers: responseHeaders,
	}

	resMsg, err := json.Marshal(responseData)
	if err != nil {
		return err
	}

	if err = channel.SendText(string(resMsg)); err != nil {
		return err
	}

	writer := &DataChannelWriter{DataChannel: channel}
	defer writer.Close()

	if _, err = io.Copy(writer, res.Body); err != nil {
		return err
	}

	return nil
}

// utils

func unmarshal2(data []byte, val any) error {
	var str string
	if err := json.Unmarshal(data, &str); err != nil {
		return err
	}

	return json.Unmarshal([]byte(str), val)
}
